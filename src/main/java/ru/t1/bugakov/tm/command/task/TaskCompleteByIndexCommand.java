package ru.t1.bugakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getAuthService().getUserId();
        getTaskService().changeTaskStatusByIndex(userId, index, Status.COMPLETED);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-complete-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Complete task by index.";
    }

}
